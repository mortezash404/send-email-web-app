﻿using System.Threading.Tasks;
using EmailWebApp.Models;
using EmailWebApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmailWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly IEmailService _service;

        public EmailController(IEmailService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post(EmailRequest emailRequest)
        {
            await _service.SendEmailAsync(emailRequest);

            return Ok();
        }
    }
}