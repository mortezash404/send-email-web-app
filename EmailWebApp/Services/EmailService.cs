﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using EmailWebApp.Models;
using Microsoft.Extensions.Options;

namespace EmailWebApp.Services
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _settings;

        public EmailService(IOptions<EmailSettings> settings)
        {
            _settings = settings.Value;
        }

        public async Task SendEmailAsync(EmailRequest emailRequest)
        {
            var message = new MailMessage();
            var smtp = new SmtpClient();

            message.From = new MailAddress(_settings.Email, _settings.DisplayName);
            message.To.Add(new MailAddress(emailRequest.ToEmail));
            message.Subject = emailRequest.Subject;
            message.IsBodyHtml = false;
            message.Body = emailRequest.Body;
            smtp.Port = _settings.Port;
            smtp.Host = _settings.Host;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(_settings.Email, _settings.Password);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            await smtp.SendMailAsync(message);
        }
    }
}
