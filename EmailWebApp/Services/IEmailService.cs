﻿using System.Threading.Tasks;
using EmailWebApp.Models;

namespace EmailWebApp.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(EmailRequest emailRequest);
    }
}